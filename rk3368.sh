# https://www.diigo.com/user/tf0054/b/413317399
# http://forum.geekbox.tv/viewtopic.php?t=174
#
# You can find a tag-name on
#   https://git.kernel.org/pub/scm/linux/kernel/git/mmind/linux-rockchip.git
#
# rootfs is based on lubuntu gotten from
#   http://cdimage.ubuntu.com/lubuntu/daily-live/20170825/
# 
docker run -i --rm --privileged -v $(pwd)/out:/tmp/out \
-e ARCH=arm64 \
-e CROSS_COMPILE="ccache aarch64-linux-gnu-" \
moul/kernel-builder \
/bin/bash -xec 'rm -rf /usr/src/linux && rm -rf /usr/src/linux-stable && \
cd /usr/src && \
apt-get update && \
apt-get -y install cpio sudo squashfs-tools && \
 true && \
echo "*** make rootfs.img (disabled) ***" && \
echo "dd if=/dev/zero of=/tmp/out/rootfs.img bs=1M count=4096 && \
mkfs.ext4 -F -L linuxroot /tmp/out/rootfs.img && \
rm -rf rootfs && mkdir rootfs && \
mount -o loop /tmp/out/rootfs.img rootfs && \
unsquashfs -f -d rootfs /tmp/out/filesystem.squashfs && \
umount rootfs && rmdir rootfs && \ " && \
 true && \
echo "*** building kernel ***" && \
git clone -b v4.12 --single-branch --depth 1 git://git.kernel.org/pub/scm/linux/kernel/git/mmind/linux-rockchip.git && \
ln -s /usr/src/linux-rockchip /usr/src/linux && \
cd /usr/src/linux && \
git log --pretty=oneline && \
make ARCH=arm64 defconfig && \
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -j2 && \
cp arch/arm64/boot/Image /tmp/out && \
 true && \
echo "*** making resource img  ***" && \
make dtbs && \
git clone https://github.com/rockchip-linux/rkbin && \
rkbin/tools/resource_tool arch/arm64/boot/dts/rockchip/rk3368-geekbox.dtb && \
cp resource.img /tmp/out && \
 true && \
echo "*** making merged ramfs (kernel+resource) ***" && \
git clone https://github.com/neo-technologies/rockchip-mkbootimg.git mkbootimg && \
make -C mkbootimg/ && \
make install -C mkbootimg/ && \
mkdir initrd && \
/usr/local/bin/mkcpiogz initrd && \
truncate -s "%4" initrd.cpio.gz && \
/usr/local/bin/mkbootimg --kernel arch/arm64/boot/Image --ramdisk initrd.cpio.gz --second resource.img -o /tmp/out/ramfs.img && \
 true && \
echo "*** (make modules) ***" && \
rm -rf /tmp/out/modules && mkdir /tmp/out/modules && \
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- modules_install INSTALL_MOD_PATH=/tmp/out/modules INSTALL_MOD_STRIP=1 && \
cat include/config/kernel.release '
