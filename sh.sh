# https://www.diigo.com/user/tf0054/b/413317399
# http://forum.geekbox.tv/viewtopic.php?t=174
#
# You can find a tag-name on
#   https://git.kernel.org/pub/scm/linux/kernel/git/mmind/linux-rockchip.git
#
# rootfs is based on lubuntu gotten from
#   http://cdimage.ubuntu.com/lubuntu/daily-live/20170825/
# 
docker run -it --rm --privileged -v $(pwd)/$1:/tmp/$1 \
-e ARCH=arm64 \
-e CROSS_COMPILE="ccache aarch64-linux-gnu-" \
kernel-builder \
/bin/bash  
