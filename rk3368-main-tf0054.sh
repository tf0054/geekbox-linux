# https://www.diigo.com/user/tf0054/b/413317399
# http://forum.geekbox.tv/viewtopic.php?t=174
#
# You can find a tag-name on
#   https://git.kernel.org/pub/scm/linux/kernel/git/mmind/linux-rockchip.git
#
# rootfs is based on lubuntu gotten from
#   http://cdimage.ubuntu.com/lubuntu/daily-live/20170825/
# 
docker run -i --rm --privileged -v $(pwd)/out:/tmp/out \
-e ARCH=arm64 \
-e CROSS_COMPILE="ccache aarch64-linux-gnu-" \
kernel-builder \
/bin/bash -xec '
echo "*** building kernel ***" && \
cd /usr/src/linux && \
git log --pretty=oneline && \
make ARCH=arm64 defconfig && \
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -j2 && \
cp arch/arm64/boot/Image /tmp/out && \
 true && \
echo "*** making resource img  ***" && \
make dtbs && \
wget https://github.com/linux-rockchip/u-boot-rockchip/raw/u-boot-rk3288/tools/resource_tool/resource_tool && \
chmod 755 resource_tool && \
./resource_tool arch/arm64/boot/dts/rockchip/rk3368-geekbox.dtb /tmp/logo.bmp && \
cp resource.img /tmp/out && \
 true && \
echo "*** making merged ramfs (kernel+resource) ***" && \
mkdir initrd && \
mkbootimg/mkcpiogz initrd && \
truncate -s "%4" initrd.cpio.gz && \
mkbootimg/mkbootimg --kernel arch/arm64/boot/Image --ramdisk initrd.cpio.gz --second resource.img -o /tmp/out/ramfs.img && \
 true && \
echo "*** (make modules) ***" && \
echo "rm -rf /tmp/out/modules && mkdir /tmp/out/modules && \
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- modules_install INSTALL_MOD_PATH=/tmp/out/modules INSTALL_MOD_STRIP=1 && \
cat include/config/kernel.release " '
